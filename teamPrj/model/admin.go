package model

import (
	"database/sql"
	"fmt"
	"teamPrj/datastore/postgres"
)

/////////////----------------------Users--------------------------////////////////////

type Users struct{
	UserName string
	Email string
	Password string
	PhoneNumber int
	Usertype string
}


const queryInsertUsers = "INSERT INTO users (username, email, password, phonenumber, usertype) VALUES($1, $2, $3, $4, $5)"

func (u *Users) Create() error {
	_, err := postgres.Db.Exec(queryInsertUsers, u.UserName, u.Email, u.Password, u.PhoneNumber, u.Usertype)
	return err
}

const queryGetUser = "SELECT usertype FROM users WHERE email = $1 AND password = $2;"

func Get(email, password string) (string, error) {
	var usertype string
	err := postgres.Db.QueryRow(queryGetUser, email, password).Scan(&usertype)
	if err != nil {
		if err == sql.ErrNoRows {
			return "", fmt.Errorf("invalid credentials")
		}
		return "", err
	}	

	return usertype, nil
}


