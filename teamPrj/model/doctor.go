package model

import "teamPrj/datastore/postgres"




type Doctor struct{
  Did int64 
  Name string 
  DOB string 
  Position string
  Department string
  Sex string
  Address string
  ContactNo int64
  Email string
  Status string
}

const (
	queryInsert = "INSERT INTO doctor(did, name, DOB, Position, Department, Sex, Address, ContactNo, email, Status) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);"
	queryGetDoctor = "SELECT did, name,DOB, Position, Department, Sex, Address, ContactNo, email, Status FROM doctor WHERE did=$1;"
	queryUpdate = "UPDATE doctor SET did=$1, name=$2, DOB=$3,Position=$4, Department=$5, Sex=$6, Address=$7, ContactNo=$8, email=$9, Status=$10 WHERE did=$11 RETURNING did;"
	queryDeleteUser = "DELETE FROM doctor WHERE did=$1 RETURNING did;"
)

func (d *Doctor) Create() error{
  // fmt.Println(d)
  _, err := postgres.Db.Exec(queryInsert, d.Did, d.Name, d.DOB, d.Position, d.Department, d.Sex, d.Address, d.ContactNo, d.Email, d.Status)
  return err
}


func (d *Doctor) Read() error{
  row:= postgres.Db.QueryRow(queryGetDoctor, d.Did) //give the vlaue of Did on your own and return the 
  err := row.Scan(&d.Did, &d.Name, &d.DOB, &d.Position, &d.Department,&d.Sex, &d.Address, &d.ContactNo, &d.Email, &d.Status)
  return err
}

func (d *Doctor) Update(oldID int64) error{
  err := postgres.Db.QueryRow(queryUpdate, d.Did, d.Name,d.DOB, &d.Position, &d.Department,&d.Sex, &d.Address, &d.ContactNo, &d.Email, &d.Status, oldID).Scan(&d.Did)
  return err
}

func (d *Doctor) Delete() error{
  if err := postgres.Db.QueryRow(queryDeleteUser, d.Did).Scan(&d.Did); err != nil{
    return err
  }
  return nil
}

func GetAllDoctors() ([]Doctor, error){
  rows, getErr := postgres.Db.Query("SELECT * FROM doctor;")
  if getErr != nil{
    return nil, getErr
  }

  doctors := []Doctor{} 

  for rows.Next() {
    var d Doctor
    dbErr := rows.Scan(&d.Did, &d.Name, &d.DOB, &d.Position, &d.Department,&d.Sex, &d.Address, &d.ContactNo, &d.Email, &d.Status)
    if dbErr != nil{
      return nil, dbErr
    }
    doctors = append(doctors, d)
  }
  rows.Close()
  return doctors, nil
}