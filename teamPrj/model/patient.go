package model

import (
	"database/sql"
	"errors"
	"teamPrj/datastore/postgres"
)

type Patient struct {
	PatientID        int64  `json:"patientId"`
	PatientName      string `json:"patientName"`
	LastName         string `json:"lastName"`
	CID              int    `json:"cid"`
	Contact          int    `json:"contact"`
	Sex              string `json:"sex"`
	Email            string `json:"email"`
	DOB              string `json:"dob"`
	PreferredDate    string `json:"preferredDate"`
	AppointmentDate  string `json:"appointmentDate"`
	DoctorID         int64  `json:"doctorId"`
}


const (
	queryAddPatient       = "INSERT INTO patient(patientid, patientname, lastname, cid, contact, sex, email, dob, preferreddate, appointmentdate, doctorid) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10,$11);"
	queryGetPatient       = "SELECT patientid, patientname, lastname, cid, contact, sex, email, dob, preferreddate, appointmentdate, doctorid FROM patient WHERE patientid = $1;"
	queryUpdatePatient    = "UPDATE patient SET patientname = $1, lastname = $2, cid = $3, contact = $4, sex = $5, email = $6, dob = $7, preferreddate=$8, appointmentdate=$9, doctorid = $10 WHERE patientid = $11;"
	queryDeletePatient    = "DELETE FROM patient WHERE patientid = $1;"
	queryGetAllPatients   = "SELECT patientid, patientname, lastname, cid, contact, sex, email, dob, preferreddate, appointmentdate, doctorid FROM patient;"

	queryGetAllPatientsByDoctorID           = "SELECT patientid, patientname, lastname, cid, contact, sex, email, dob, preferreddate, appointmentdate, doctorid FROM patient WHERE doctorid = $1;"
	queryGetPatientByDoctorIDAndAppointmentDate = "SELECT patientid, patientname, lastname, cid, contact, sex, email, dob, preferreddate, appointmentdate, doctorid FROM patient WHERE doctorid = $1 AND appointmentdate = $2;"
)

func AddPatient(patient *Patient) error {
	_, err := postgres.Db.Exec(queryAddPatient, patient.PatientID, patient.PatientName, patient.LastName, patient.CID, patient.Contact, patient.Sex, patient.Email, patient.DOB, patient.PreferredDate, patient.AppointmentDate, patient.DoctorID)
	return err
}

func GetPatient(patientID int64) (*Patient, error) {
	patient := &Patient{}
	err := postgres.Db.QueryRow(queryGetPatient, patientID).Scan(&patient.PatientID, &patient.PatientName, &patient.LastName, &patient.CID, &patient.Contact, &patient.Sex, &patient.Email, &patient.DOB, &patient.PreferredDate, &patient.AppointmentDate, &patient.DoctorID)
	if err != nil {
		return nil, err
	}
	return patient, nil
}

func UpdatePatient(patient *Patient) error {
	_, err := postgres.Db.Exec(queryUpdatePatient, patient.PatientName, patient.LastName, patient.CID, patient.Contact, patient.Sex, patient.Email, patient.DOB, patient.PreferredDate, patient.AppointmentDate, patient.DoctorID, patient.PatientID)
	return err
}

func DeletePatient(patientID int64) error {
	_, err := postgres.Db.Exec(queryDeletePatient, patientID)
	return err
}

func GetAllPatients() ([]*Patient, error) {
	rows, err := postgres.Db.Query(queryGetAllPatients)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	patients := []*Patient{}
	for rows.Next() {
		patient := &Patient{}
		err := rows.Scan(&patient.PatientID, &patient.PatientName, &patient.LastName, &patient.CID, &patient.Contact, &patient.Sex, &patient.Email, &patient.DOB, &patient.PreferredDate, &patient.AppointmentDate, &patient.DoctorID)
		if err != nil {
			return nil, err
		}
		patients = append(patients, patient)
	}
	return patients, nil
}



func GetAllPatientsByDoctorID(doctorID int64) ([]*Patient, error) {
	rows, err := postgres.Db.Query(queryGetAllPatientsByDoctorID, doctorID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	patients := []*Patient{}
	for rows.Next() {
		patient := &Patient{}
		err := rows.Scan(&patient.PatientID, &patient.PatientName, &patient.LastName, &patient.CID, &patient.Contact, &patient.Sex, &patient.Email, &patient.DOB, &patient.PreferredDate, &patient.AppointmentDate, &patient.DoctorID)
		if err != nil {
			return nil, err
		}
		patients = append(patients, patient)
	}
	return patients, nil
}

func GetPatientByDoctorIDAndAppointmentDate(doctorID int64, appointmentDate string) (*Patient, error) {
	patient := &Patient{}
	err := postgres.Db.QueryRow(queryGetPatientByDoctorIDAndAppointmentDate, doctorID, appointmentDate).Scan(&patient.PatientID, &patient.PatientName, &patient.LastName, &patient.CID, &patient.Contact, &patient.Sex, &patient.Email, &patient.DOB, &patient.PreferredDate, &patient.AppointmentDate, &patient.DoctorID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil // Patient not found
		}
		return nil, err
	}
	return patient, nil
}