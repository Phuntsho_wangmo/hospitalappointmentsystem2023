package model

import (
	"database/sql"
	"fmt"
	"teamPrj/datastore/postgres"
)

type Appointment struct {
	AppointmentID int
	FirstName     string
	LastName      string
	CID           int64
	Phone         int
	Gender        string
	Email         string
	DOB           string
	PreferredDate string
}

const queryInsertAppointment = "INSERT INTO Appointments (FirstName, LastName, CID, Phone, Gender, Email, DOB, PreferredDate) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)"

func (a *Appointment) Create(cid int64) error {
	_, err := postgres.Db.Exec(queryInsertAppointment, a.FirstName, a.LastName, cid, a.Phone, a.Gender, a.Email, a.DOB, a.PreferredDate)
	return err
}




const queryGetAppointmentByID = "SELECT AppointmentID, FirstName, LastName, CID, Phone, Gender, Email, DOB, PreferredDate FROM Appointments WHERE AppointmentID = $1"

func GetAppointmentByID(appointmentID int) (*Appointment, error) {
	appointment := &Appointment{}
	err := postgres.Db.QueryRow(queryGetAppointmentByID, appointmentID).Scan(&appointment.AppointmentID, &appointment.FirstName, &appointment.LastName, &appointment.CID, &appointment.Phone, &appointment.Gender, &appointment.Email, &appointment.DOB, &appointment.PreferredDate)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("appointment not found")
		}
		return nil, err
	}
	return appointment, nil
}

const queryGetAllAppointments = "SELECT AppointmentID, FirstName, LastName, CID, Phone, Gender, Email, DOB, PreferredDate FROM Appointments"

func GetAllAppointments() ([]*Appointment, error) {
	rows, err := postgres.Db.Query(queryGetAllAppointments)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	appointments := []*Appointment{}
	for rows.Next() {
		appointment := &Appointment{}
		err := rows.Scan(&appointment.AppointmentID, &appointment.FirstName, &appointment.LastName, &appointment.CID, &appointment.Phone, &appointment.Gender, &appointment.Email, &appointment.DOB, &appointment.PreferredDate)
		if err != nil {
			return nil, err
		}
		appointments = append(appointments, appointment)
	}

	return appointments, nil
}

const queryUpdateAppointment = "UPDATE Appointments SET FirstName = $1, LastName = $2, CID = $3, Phone = $4, Gender = $5, Email = $6, DOB = $7, PreferredDate=$8 WHERE AppointmentID = $9"

func (a *Appointment) Update() error {
	_, err := postgres.Db.Exec(queryUpdateAppointment, a.FirstName, a.LastName, a.CID, a.Phone, a.Gender, a.Email, a.DOB, a.PreferredDate, a.AppointmentID)
	return err
}

const queryDeleteAppointment = "DELETE FROM Appointments WHERE AppointmentID = $1"

func DeleteAppointment(appointmentID int) error {
	_, err := postgres.Db.Exec(queryDeleteAppointment, appointmentID)
	return err
}
