const name = document.querySelector("#firstname");
const CID = document.querySelector("#cid");
const phoneno = document.querySelector("#phonenumber");
const email1 = document.querySelector("#email");
const dob = document.querySelector("#date_time");
const place = document.querySelector("#place");

form.addEventListener("submit", function (e) {
  e.preventDefault();
  let isnameValid = checkname(),
    isCIDValid = checkCID(),
    isphonenoValid = checkphoneno(),
    isemail1Valid = checkEmail(),
    isdobValid = checkDatet(),
    isplaceValid = checkplace();

  let isFormValid =
    isnameValid &&
    isCIDValid &&
    isphonenoValid &&
    isemail1Valid &&
    isdobValid &&
    isplaceValid;

  if (isFormValid) {
    window.Location.href = "submit.html";
  }
});

//for name
function checkname() {
  let valid = false;
  const min = 3,
    max = 25;
  const ename = ename.value.trim();
  if (!isRequired(ename)) {
    showError(ename, "name cannot be blank.");
  } else if (!isBetween(ename.length, min, max)) {
    showError(ename, `name must be between ${min} and ${max} characters.`);
  } else {
    showSuccess(ename);
    valid = true;
  }
  return valid;
}

let isRequired = (value) => (value === "" ? false : true);

const isBetween = (length, min, max) =>
  length < min || length > max ? false : true;

let showError = (input, message) => {
  let formField = input.parentElement;
  formField.classList.remove("success");
  formField.classList.add("error");

  const error = formField.querySelector("small");
  error.textContent = message;
};

const showSuccess = (input) => {
  const formField = input.parentElement;
  formField.classList.remove("error");
  formField.classList.add("success");
  formField.querySelector("small").textContent = "";
};

let isPositionRequired = (value) => (value === "Default" ? false : true);

//for cid
function checkCID() {
  let valid = false;
  const usercid = CID.value.trim();
  if (!isRequired(usercid)) {
    showError(phoneno, "CID cannot be blank.");
  } else if (!isEidValid(usercid)) {
    showError(phoneno, `CID can only be numbers`);
  } else {
    showSuccess(phoneno);
    valid = true;
  }
  return valid;
}

//for phone no
function checkphoneno() {
  let valid = false;
  const userphoneno = phoneno.value.trim();
  if (!isRequired(userphoneno)) {
    showError(phoneno, "phone number cannot be blank.");
  } else if (!isEidValid(userphoneno)) {
    showError(phoneno, `Phone number can only be numbers`);
  } else {
    showSuccess(phoneno);
    valid = true;
  }
  return valid;
}

const isphonenoValid = (number) => {
  const re = /^([0-9])+$/;
  return re.test(number);
};

const isemail1Valid = (email) => {
  const re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

//for email
function checkEmail() {
  let valid = false;
  const email = email1.value.trim();
  if (!isRequired(email)) {
    showError(email1, " Email cannot be blank.");
  } else if (!isemail1Valid(email)) {
    showError(email1, `Email is not valid`);
  } else {
    showSuccess(email1);
    valid = true;
  }
  return valid;
}

//for dob
function checkDatet() {
  let valid = false;
  const Dateofbirth = dob.value.trim();
  if (!isRequired(Dateofbirth)) {
    showError(dob, "Date of birth cannot be blank.");
  } else {
    showSuccess(dob);
    valid = true;
  }
  return valid;
}

//for place
function checkplace() {
  let valid = false;
  const address = place.value.trim();
  if (!isRequired(address)) {
    showError(place, "Address cannot be blank.");
  } else {
    showSuccess(place);
    valid = true;
  }
  return valid;
}
