fetch('/doctors')
  .then(response => response.json())
  .then(doctorsData => {
    // Process the list of doctors' data
    // Find the specific doctor's data based on the logged-in doctor's information
    const loggedInDoctor = doctorsData.find(doctor => doctor.Email === usersession.Email);
    
    // Display the doctor's details on the page
    displayDoctorDetails(loggedInDoctor);
  })
  .catch(error => {
    console.error('Error fetching doctors data:', error);
  });


  function displayDoctorDetails(doctor) {
    const didElement = document.getElementById('did');
    const dnameElement = document.getElementById('dname');
    const dobElement = document.getElementById('dob');
    const contactElement = document.getElementById('contact');
  
    if (doctor) {
      didElement.textContent = doctor.did;
      dnameElement.textContent = doctor.name;
      dobElement.textContent = doctor.DOB;
      contactElement.textContent = doctor.ContactNo;
    }
  }
  