function login() {
  var email = document.getElementById("loginEmail").value;
  var password = document.getElementById("loginPassword").value;

  var loginData = {
    email: email,
    password: password,
  };

  fetch("/loginusers", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(loginData),
  })
    .then((response) => {
      if (response.ok) {
        // Handle successful login response
        return response.json();
      } else {
        throw new Error(response.statusText);
      }
    })
    .then((data) => {
      // Handle response data
      if (data.status === "user login success") {
        // Save email in localStorage to indicate the user is logged in
        localStorage.setItem('email', email);

        // Redirect to the appropriate page based on user type retrieved from the database
        var userType = data.userType;

        if (userType === "admin") {
          window.open("dash.html", "_self");
        } else if (userType === "user") {
          window.open("home.html", "_self");
        } else if (userType === "doctor") {
          window.open("doctorhome.html", "_self");
        } else {
          // Invalid user type
          alert("Please select a valid user type");
          console.log(userType)
        }
      } else {
        // Handle other response cases or errors
        // For example, display an error message if login failed
        alert("Login failed. Please try again.");
      }
    })
    .catch((error) => {
      // Handle error cases
      console.log("Login error:", error);
      alert("Credential does not match. Try again");
    });
}
