function userSignUp() {
  var data = {
    username: document.getElementById("username").value,
    email: document.getElementById("email").value,
    password: document.getElementById("password").value,
    pw: document.getElementById("confirm_password").value,
    phonenumber: Number(document.getElementById("phonenumberSignup").value),
    usertype: "user",
  };

  if (data.password !== data.pw) {
    alert("PASSWORD doesn't match!");
    return;
  }

  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(data.email)) {
    alert("Invalid email format");
    return;
  }

  const phoneNumberPattern = (/^(17|77)\d{6}$/);
  if (!phoneNumberPattern.test(data.phonenumber)) {
    alert("Invalid phone number");
    return;
  }

  fetch("/signupusers", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data),
  }).then((response) => {
    if (response.status === 201) {
      window.open("home.html", "_self");
    }
  });
}
