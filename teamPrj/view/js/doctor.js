window.onload = function() {
    fetch("/doctors")
      .then(response => response.json())
      .then(data => {
        console.log('Fetched doctors:', data);
        showDoctors(data);
      })
      .catch(error => {
        console.error('Error fetching doctors:', error);
      });
  };
  
  function addDoctor() {
    var data = {
      did: Number(document.getElementById('doctorId').value),
      name: document.getElementById('doctorName').value,  
      dob: document.getElementById('DOB').value,
      position: document.getElementById('position').value,
      department: document.getElementById('department').value,
      sex: document.getElementById('sex').value,
      address: document.getElementById('address').value,
      contactno: Number(document.getElementById('contact').value),
      email: document.getElementById('email').value,
      status: document.getElementById('Status').value,

    };

    // alert(JSON.stringify(data));
    var did = data.did;
  
    fetch("/doctor", {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    })
      .then(response1 => {
        if (response1.ok) {
          fetch("/doctor/" + did)
            .then(response2 => response2.json())
            .then(data => showdoctor(data))
            .catch(error => {
              console.error('Error fetching doctor:', error);
            });
        } else {
          throw new Error(response1.statusText);
        }
      })
      .catch(error => {
        console.error('Error creating doctor:', error);
        // alert(error);
      });
  
    // resetForm();
  }
  
  function showdoctor(data) {
    newRow(data);
  }
  
  function resetForm() {
      document.getElementById('doctorId').value = " ";
      document.getElementById('doctorName').value = " ";
      document.getElementById('DOB').value = " ";
      document.getElementById('position').value = " ";
      document.getElementById('department').value = " ";
      document.getElementById('sex').value = " ";
      document.getElementById('address').value = " ";
      document.getElementById('contact').value = " ";
      document.getElementById('email').value = " ";
      document.getElementById('Status').value = " ";
  }
  
  function showDoctors(data) {
    data.forEach(doctor => {
      newRow(doctor);
    });
  }
  
  function newRow(doctor) {
    var table = document.getElementById("doctorList");
    var row = table.insertRow(table.length);
  
    var td = [];
    for (var i = 0; i < table.rows[0].cells.length; i++) {
      td[i] = row.insertCell(i);
    }

  
    td[0].innerHTML = doctor.Did;
    td[1].innerHTML = doctor.Name;
    td[2].innerHTML = doctor.DOB.split("T")[0];
    td[3].innerHTML = doctor.Position;
    td[4].innerHTML = doctor.Department;
    td[5].innerHTML = doctor.Sex;
    td[6].innerHTML = doctor.Address;
    td[7].innerHTML = doctor.ContactNo;
    td[8].innerHTML = doctor.Email;
    td[9].innerHTML = doctor.Status;

  
    td[10].innerHTML = '<input type="button" onclick="deleteDoctor(this)" value="Delete" id="button-1">';
    td[11].innerHTML = '<input type="button" onclick="updateDoctor(this)" value="Edit" id="button-2">';
  }
  
  var selectedRow = null;

  function updateDoctor(r) {
    selectedRow = r.parentElement.parentElement;
  
    document.getElementById('doctorId').value = selectedRow.cells[0].innerHTML;
    document.getElementById('doctorName').value = selectedRow.cells[1].innerHTML;
    document.getElementById('DOB').value = selectedRow.cells[2].innerHTML;
    document.getElementById('position').value = selectedRow.cells[3].innerHTML;
    document.getElementById('department').value = selectedRow.cells[4].innerHTML;
    document.getElementById('sex').value = selectedRow.cells[5].innerHTML;
    document.getElementById('address').value = selectedRow.cells[6].innerHTML;
    document.getElementById('contact').value = selectedRow.cells[7].innerHTML;
    document.getElementById('email').value = selectedRow.cells[8].innerHTML;
    document.getElementById('Status').value = selectedRow.cells[9].innerHTML;
  
    var btn = document.getElementById("button-add");
    did = selectedRow.cells[0].innerHTML;
  
    if (btn) {
      btn.innerHTML = "Update";
      btn.setAttribute("onclick", "update(did)");
    }
  }
  

  function update(did) {
    var newData = getFormData();
  
    fetch("/doctor/" + did, {
      method: "PUT",
      body: JSON.stringify(newData),
      headers: { "Content-Type": "application/json" },
    })
      .then(res => {
        if (res.ok) {
          selectedRow.cells[0].innerHTML = newData.Did;
          selectedRow.cells[1].innerHTML = newData.Name;
          selectedRow.cells[2].innerHTML = newData.DOB.split("T")[0];
          selectedRow.cells[3].innerHTML = newData.Position;
          selectedRow.cells[4].innerHTML = newData.Department;
          selectedRow.cells[5].innerHTML = newData.Sex;
          selectedRow.cells[6].innerHTML = newData.Address;
          selectedRow.cells[7].innerHTML = newData.ContactNo;
          selectedRow.cells[8].innerHTML = newData.Email;
          selectedRow.cells[9].innerHTML = newData.Status;
  
          var btn = document.getElementById("button-add");
          did = selectedRow.cells[0].innerHTML;
  
          if (btn) {
            btn.innerHTML = "Add";
            btn.setAttribute("onclick", "addDoctor()");
            selectedRow = null;
            resetForm();
          }
        } else {
          // alert(JSON.stringify(newData))

          alert("Server: Update request error");
        }
      })
      .catch(error => {
        console.error('Error updating doctorList:', error);
        // alert(error);
      });
  }
  


  
  function getFormData() {
    var formdata = {
      did: Number(document.getElementById('doctorId').value),
      name: document.getElementById('doctorName').value,
      dob: document.getElementById('DOB').value,
      position: document.getElementById('position').value,
      department: document.getElementById('department').value,
      sex: document.getElementById('sex').value,
      address: document.getElementById('address').value,
      contactno: Number(document.getElementById('contact').value),
      email: document.getElementById('email').value,
      status: document.getElementById('Status').value,

    };
    return formdata;
  }
  


  function deleteDoctor(r) {
    if (confirm("Are you sure to delete this doctor info?")) {
      selectedRow = r.parentElement.parentElement;
      did = selectedRow.cells[0].innerHTML;
  
      fetch("/doctor/" + did, {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
      })
        .then(() => {
          var rowIndex = selectedRow.rowIndex;
          if (rowIndex > 0) {
            document.getElementById("doctorList").deleteRow(rowIndex);
          }
          selectedRow = null;
        })
        .catch(error => {
          console.error('Error deleting doctorList:', error);
          // alert(error);
        });
    }
  }
  