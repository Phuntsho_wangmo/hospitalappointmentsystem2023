

window.onload = function() {
  fetch("/doctors")
    .then(response => response.json())
    .then(data => getDoctors(data))
    .then(() => fetch("/patients"))
    .then(response => response.json())
    .then(data => {
      console.log('Fetched patients:', data);
      showPatients(data);
    })
    .catch(error => {
      console.error('Error:', error);
    });
};

function getDoctors(data) {
  const select = document.getElementById("doctorid");
  data.forEach(doctor => {
    const option = document.createElement("option");
    option.textContent = doctor.Did;
    option.value = doctor.Did;
    select.appendChild(option);
  });
}

function addPatient() {
  const data = {
    patientId: Number(document.getElementById('patientId').value),
    patientname: document.getElementById('patientName').value,
    lastname: document.getElementById('Name').value,
    cid: Number(document.getElementById('CID').value),
    contact: Number(document.getElementById('contact').value),
    sex: document.getElementById('sex').value,
    email: document.getElementById('email').value,
    dob: document.getElementById('DOB').value,
    preferreddate: document.getElementById('preferabledate').value,
    appointmentdate: document.getElementById('date_time').value,
    doctorId: Number(document.getElementById('doctorid').value)
  };

  const pid = data.patientId;
  fetch("/patient", {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-Type": "application/json" },
  })
    .then(response => {
      if (response.ok) {
        fetch("/patient/" + pid)
          .then(response => response.json())
          .then(data => {
            showPatient(data); 

            const appointmentDate = data.appointmentDate;

            // Display or use the appointment date as needed
            // alert(data.appointmentdate);
  
            // Save the appointment date in localStorage
            localStorage.setItem('appointmentDate', appointmentDate);
  
            // Redirect to doctorprofile.html
            // window.location.href = 'userprofile.html';
          })

          .catch(error => {
            console.error('Error fetching appointment:', error);
          });
      } else {
        throw new Error(response.statusText);
      }
    })
    .catch(error => {
      console.error('Error creating appointment:', error);
      // alert(error);
    });
  
  resetForm();
  
}
  
  
  function showPatient(data) {
    newRow(data);
  }
  
  function resetForm() {
    document.getElementById('patientId').value = '';
    document.getElementById('patientName').value = '';
    document.getElementById('Name').value = '';
    document.getElementById('CID').value = '';
    document.getElementById('contact').value = '';
    document.getElementById('sex').value = '';
    document.getElementById('email').value = '';
    document.getElementById('DOB').value = '';
    document.getElementById('preferabledate').value = '';
    document.getElementById('date_time').value = '';
    document.getElementById('doctorid').value = " "; 
  }
  
  function showPatients(data) {
    data.forEach(patient => {
      newRow(patient);
    });
  }

  
  function newRow(patient) {
    var table = document.getElementById("myTable");
    var row = table.insertRow(table.length);
  
    var td = [];
    for (var i = 0; i < table.rows[0].cells.length; i++) {
      td[i] = row.insertCell(i);
    }
  
    td[0].innerHTML = patient.patientId;
    td[1].innerHTML = patient.patientName;
    td[2].innerHTML = patient.lastName;
    td[3].innerHTML = patient.cid;
    td[4].innerHTML = patient.contact;
    td[5].innerHTML = patient.sex;
    td[6].innerHTML = patient.email;
    td[7].innerHTML = patient.dob.split("T")[0];
    td[8].innerHTML = patient.preferredDate.split("T")[0];
    td[9].innerHTML = patient.appointmentDate;
    td[10].innerHTML = patient.doctorId;
  
    td[11].innerHTML = '<input type="button" onclick="deletePatient(this)" value="Delete" id="button-1">';
    td[12].innerHTML = '<input type="button" onclick="updatePatient(this)" value="Edit" id="button-2">';
  }

    
  function getFormData() {
    var formdata = {
      patientId: Number(document.getElementById('patientId').value),
      patientname: document.getElementById('patientName').value,
      lastname: document.getElementById('Name').value,
      cid: Number(document.getElementById('CID').value),
      contact: Number(document.getElementById('contact').value),
      sex: document.getElementById('sex').value,
      email: document.getElementById('email').value,
      dob: document.getElementById('DOB').value,
      preferreddate: document.getElementById('preferabledate').value,
      appointmentdate: document.getElementById('date_time').value,
      doctorId: Number(document.getElementById('doctorid').value),

    };
    return formdata;
  }
  

  
  var selectedRow = null;
  
  function updatePatient(r) {
    selectedRow = r.parentElement.parentElement;
  
    document.getElementById('patientId').value = selectedRow.cells[0].innerHTML;
    document.getElementById('patientName').value = selectedRow.cells[1].innerHTML;
    document.getElementById('Name').value = selectedRow.cells[2].innerHTML;
    document.getElementById('CID').value = selectedRow.cells[3].innerHTML;
    document.getElementById('contact').value = selectedRow.cells[4].innerHTML;
    document.getElementById('sex').value = selectedRow.cells[5].innerHTML;
    document.getElementById('email').value = selectedRow.cells[6].innerHTML;
    document.getElementById('DOB').value = selectedRow.cells[7].innerHTML;
    document.getElementById('preferabledate').value = selectedRow.cells[8].innerHTML;
    document.getElementById('date_time').value = selectedRow.cells[9].innerHTML;
    document.getElementById('doctorid').value = selectedRow.cells[10].innerHTML;

    
  
    var btn = document.getElementById("button-add");
    pid = selectedRow.cells[0].innerHTML;
    if (btn) {
      // resetForm();

      btn.innerHTML = "Update";
      btn.setAttribute("onclick", "update(pid)");
      
    }
  }
  
  function update(pid) {
    var newData = getFormData();  
    newData.appointmentdate = document.getElementById('date_time').value;
    fetch("/patient/" + pid, {
      method: "PUT",
      body: JSON.stringify(newData),
      headers: { "Content-Type": "application/json" },
    })
      .then(res => {
        if (res.ok) {
          selectedRow.cells[0].innerHTML = newData.patientId;
          selectedRow.cells[1].innerHTML = newData.patientName;
          selectedRow.cells[2].innerHTML = newData.lastName;
          selectedRow.cells[3].innerHTML = newData.cid;
          selectedRow.cells[4].innerHTML = newData.contact;
          selectedRow.cells[5].innerHTML = newData.sex;
          selectedRow.cells[6].innerHTML = newData.email;
          selectedRow.cells[7].innerHTML = newData.dob.split("T")[0];
          selectedRow.cells[8].innerHTML = newData.preferredDate.split("T")[0];
          selectedRow.cells[9].innerHTML = newData.appointmentDate;
          selectedRow.cells[10].innerHTML = newData.doctorId;

  
          var btn = document.getElementById("button-add");

          pid = selectedRow.cells[0].innerHTML;
          if (btn) {
            btn.innerHTML = "Add";
            btn.setAttribute("onclick", "addPatient()");
            selectedRow = null;
            resetForm();
          }
        } else {
          // console.log(error);
          // alert(JSON.stringify(newData))

          throw new Error("Server: Update request error");
     
        }
      })
      .catch(error => {
        console.error('Error updating appointment:', error);
        alert(error);
      });
  }
  



  function deletePatient(r) {
    if (confirm("Are you sure to delete this patient info?")) {
      selectedRow = r.parentElement.parentElement;
      pid = selectedRow.cells[0].innerHTML;
  
      fetch("/patient/" + pid, {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
      })
        .then(() => {
          var rowIndex = selectedRow.rowIndex;
          if (rowIndex > 0) {
            document.getElementById("myTable").deleteRow(rowIndex);
          }
          selectedRow = null;
        })
        .catch(error => {
          console.error('Error deleting appointment:', error);
          alert(error);
        });
    }
  }
  
