
function createAppointment() {
  // Retrieve form inputs
  const firstName = document.getElementById('firstname').value;
  const lastName = document.getElementById('lastname').value;
  const CID = Number(document.getElementById('cid').value);
  const phone = Number(document.getElementById('phonenumber').value);
  const gender = document.querySelector('input[name="gender"]:checked').value;
  const email = document.getElementById('email').value;
  const dob = document.getElementById('date_time').value;
  const date = document.getElementById('preferabledate').value;


  // Create appointment object
  const appointmentData = {
    FirstName: firstName,
    LastName: lastName,
    CID: CID,
    Phone: phone,
    Gender: gender,
    Email: email,
    DOB: dob,
    PreferredDate: date,
  };
  // Send appointment request to the server
  fetch('/appointments', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(appointmentData),
  })
    .then(response => response.json())
    .then(data => { 
      console.log(JSON.stringify(data)); // Log the response message from the server
      // Handle the response as needed
    })
    .catch(error => {
      console.error('Error creating appointment:', error);
      console.log(error)
    });
}


function fetchAppointments() {
  fetch("/appointments")
    .then(response => response.json())
    .then(data => {
      // Process the fetched appointments data
      console.log(data);
      // Update the UI with the fetched appointments
      var appointmentsTable = document.getElementById('appointmentsTable');
      appointmentsTable.innerHTML = '';

      // Iterate over the fetched appointments and create table rows
      data.forEach(appointment => {
        var tableRow = document.createElement('tr');
        tableRow.innerHTML = `
          <td>${appointment.FirstName} send an appointment request</td>
        `;

        tableRow.addEventListener('click', () => {
          redirectToAppointmentForm(appointment);
        });

        tableRow.style.cursor = 'pointer';
        appointmentsTable.appendChild(tableRow);
      });
    })
    .catch(error => {
      console.error('Error fetching appointments:', error);
    });
}

function redirectToAppointmentForm(appointment) {
  // Convert the appointment data to a query string
  const queryString = Object.keys(appointment)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(appointment[key])}`)
    .join('&');

  // Redirect to the other page with the query string as parameters
  // alert(JSON.stringify(appointment));
  window.location.href = `patient2.html?${queryString}`;

}