const sessionExists = !!localStorage.getItem('email');

if (!sessionExists) {
    alert("Please Login")
    location.assign("index.html")
}