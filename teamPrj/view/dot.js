var selectedRow = null;

function onFormSubmit(e) {
  event.preventDefault();
  if (validate()) {
    var formData = readFormData();

    if (selectedRow === null) {
      insertNewRecord(formData);
    } else {
      updateRecord(formData);
    }

    resetForm();
  }
}

// Read operation using this function

function readFormData() {
  var formData = {};

  formData["doctorId"] = document.getElementById("doctorId").value;

  formData["doctorName"] = document.getElementById("doctorName").value;

  formData["DOB"] = document.getElementById("DOB").value;

  formData["position"] = document.getElementById("position").value;

  formData["department"] = document.getElementById("department").value;

  formData["sex"] = document.getElementById("sex").value;

  formData["address"] = document.getElementById("address").value;

  formData["contact"] = document.getElementById("contact").value;

  formData["email"] = document.getElementById("email").value;

  formData["Status"] = document.getElementById("Status").value;

  return formData;
}

// Create operation

function insertNewRecord(data) {
  var table = document
    .getElementById("doctorList")
    .getElementsByTagName("tbody")[0];

  var newRow = table.insertRow(table.length);

  var cell1 = newRow.insertCell(0);

  cell1.innerHTML = data.doctorId;

  var cell2 = newRow.insertCell(1);

  cell2.innerHTML = data.doctorName;

  var cell3 = newRow.insertCell(2);

  cell3.innerHTML = data.DOB;

  var cell4 = newRow.insertCell(3);

  cell4.innerHTML = data.position;

  var cell5 = newRow.insertCell(4);

  cell5.innerHTML = data.department;

  var cell6 = newRow.insertCell(5);

  cell6.innerHTML = data.sex;

  var cell7 = newRow.insertCell(6);

  cell7.innerHTML = data.address;

  var cell8 = newRow.insertCell(7);

  cell8.innerHTML = data.contact;

  var cell9 = newRow.insertCell(8);

  cell9.innerHTML = data.email;

  var cell10 = newRow.insertCell(9);

  cell10.innerHTML = data.Status;

  var cell11 = newRow.insertCell(10);

  cell11.innerHTML = `<a href="#" onClick='onEdit(this)'>Edit</a>

                        <a href="#" onClick='onDelete(this)'>Delete</a>`;
}

// To Reset the data of fill input

function resetForm() {
  document.getElementById("doctorId").value = "";

  document.getElementById("doctorName").value = "";

  document.getElementById("DOB").value = "";

  document.getElementById("position").value = "";

  document.getElementById("department").value = "";

  document.getElementById("sex").value = "";

  document.getElementById("address").value = "";

  document.getElementById("contact").value = "";

  document.getElementById("email").value = "";

  document.getElementById("Status").value = "";

  selectedRow = null;
}

// For Edit operation

function onEdit(td) {
  selectedRow = td.parentElement.parentElement;

  document.getElementById("doctorId").value = selectedRow.cells[0].innerHTML;

  document.getElementById("doctorName").value = selectedRow.cells[1].innerHTML;

  document.getElementById("DOB").value = selectedRow.cells[2].innerHTML;

  document.getElementById("position").value = selectedRow.cells[3].innerHTML;

  document.getElementById("department").value = selectedRow.cells[4].innerHTML;

  document.getElementById("sex").value = selectedRow.cells[5].innerHTML;

  document.getElementById("address").value = selectedRow.cells[6].innerHTML;

  document.getElementById("contact").value = selectedRow.cells[7].innerHTML;

  document.getElementById("email").value = selectedRow.cells[8].innerHTML;

  document.getElementById("Status").value = selectedRow.cells[9].innerHTML;
}

function updateRecord(formData) {
  selectedRow.cells[0].innerHTML = formData.doctorId;

  selectedRow.cells[1].innerHTML = formData.doctorName;

  selectedRow.cells[2].innerHTML = formData.DOB;

  selectedRow.cells[3].innerHTML = formData.position;

  selectedRow.cells[4].innerHTML = formData.department;

  selectedRow.cells[5].innerHTML = formData.sex;

  selectedRow.cells[6].innerHTML = formData.address;

  selectedRow.cells[7].innerHTML = formData.contact;

  selectedRow.cells[8].innerHTML = formData.email;

  selectedRow.cells[9].innerHTML = formData.Status;
}

function onDelete(td) {
  if (confirm("Are you sure you want to delete this record?")) {
    row = td.parentElement.parentElement;

    document.getElementById("doctorList").deleteRow(row.rowIndex);

    resetForm();
  }
}

function validate() {
  isValid = true;
  if (document.getElementById("doctorId").value == "") {
    isValid = false;
    document.getElementById("idValidationError").classList.remove("hide");
  } else {
    isValid = true;
    if (
      document.getElementById("idValidationError").classList.contains("hide")
    );
    document.getElementById("idValidationError").classList.add("hide");
  }
  if (document.getElementById("doctorName").value == "") {
    isValid = false;
    document.getElementById("nameValidationError").classList.remove("hide");
  } else {
    isValid = true;
    if (
      document.getElementById("nameValidationError").classList.contains("hide")
    );
    document.getElementById("nameValidationError").classList.add("hide");
  }
  if (document.getElementById("department").value == "") {
    isValid = false;
    document
      .getElementById("departmentValidationError")
      .classList.remove("hide");
  } else {
    isValid = true;
    if (
      document
        .getElementById("departmentValidationError")
        .classList.contains("hide")
    );
    document.getElementById("departmentValidationError").classList.add("hide");
  }
  if (document.getElementById("sex").value == "") {
    isValid = false;
    document.getElementById("sexValidationError").classList.remove("hide");
  } else {
    isValid = true;
    if (
      document.getElementById("sexValidationError").classList.contains("hide")
    );
    document.getElementById("sexValidationError").classList.add("hide");
  }
  if (document.getElementById("address").value == "") {
    isValid = false;
    document.getElementById("addressValidationError").classList.remove("hide");
  } else {
    isValid = true;
    if (
      document
        .getElementById("addressValidationError")
        .classList.contains("hide")
    );
    document.getElementById("addressValidationError").classList.add("hide");
  }
  if (document.getElementById("contact").value == "") {
    isValid = false;
    document.getElementById("contactValidationError").classList.remove("hide");
  } else {
    isValid = true;
    if (
      document
        .getElementById("contactValidationError")
        .classList.contains("hide")
    );
    document.getElementById("contactValidationError").classList.add("hide");
  }
  if (document.getElementById("email").value == "") {
    isValid = false;
    document.getElementById("emailValidationError").classList.remove("hide");
  } else {
    isValid = true;
    if (
      document.getElementById("emailValidationError").classList.contains("hide")
    );
    document.getElementById("emailValidationError").classList.add("hide");
  }
  return isValid;
}
