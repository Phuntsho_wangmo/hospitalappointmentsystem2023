package routes

import (
	"log"
	"net/http"
	"os"
	"teamPrj/controller"

	"github.com/gorilla/mux"
)

func InitialisedRoutes() {
	
// gorilla mux router creating
router := mux.NewRouter()
// =================Admin=========================

router.HandleFunc("/signupusers", controller.SignupUsers).Methods("POST")
// router.HandleFunc("/signupdoctor", controller.SignupDoctor).Methods("POST")
// router.HandleFunc("/signupadmin", controller.SignupAdmin).Methods("POST")
router.HandleFunc("/loginusers", controller.LoginUsers).Methods("POST")
// router.HandleFunc("/logindoctor", controller.LoginDoctor).Methods("POST")
// router.HandleFunc("/loginadmin", controller.LoginAdmin).Methods("POST")


router.HandleFunc("/appointments", controller.CreateAppointment).Methods("POST")
router.HandleFunc("/appointments/{id}", controller.GetAppointmentByID).Methods("GET")
router.HandleFunc("/appointments", controller.GetAllAppointments).Methods("GET")
router.HandleFunc("/appointments/{id}", controller.UpdateAppointment).Methods("PUT")
router.HandleFunc("/appointments/{id}", controller.DeleteAppointment).Methods("DELETE")


// ####################################doctor##################################
router.HandleFunc("/doctor", controller.AddDoctor).Methods("POST")
router.HandleFunc("/doctor/{did}", controller.GetDoc).Methods("GET")
router.HandleFunc("/doctor/{did}", controller.UpdateDoc).Methods("PUT")
router.HandleFunc("/doctor/{did}", controller.DeleteDoc).Methods("DELETE")
router.HandleFunc("/doctors", controller.GetAllDocs)


router.HandleFunc("/doctor/{doctorID}/patients", controller.GetAllPatientsByDoctorID).Methods("GET")
router.HandleFunc("/doctor/{doctorID}/patients/{appointmentDate}", controller.GetPatientByDoctorIDAndAppointmentDate).Methods("GET")


// ####################################Patient##################################
router.HandleFunc("/patient", controller.AddPatient).Methods("POST")
router.HandleFunc("/patient/{pid}", controller.GetPatient).Methods("GET")
router.HandleFunc("/patient/{pid}", controller.UpdatePatient).Methods("PUT")
router.HandleFunc("/patient/{pid}", controller.DeletePatient).Methods("DELETE")
router.HandleFunc("/patients", controller.GetAllPatients)


// to serve static file...to run the html 
fhandler := http.FileServer(http.Dir("./view"))
router.PathPrefix("/").Handler(fhandler)

	log.Println("Application running on port 1110....") //just to print somemess

err := http.ListenAndServe(":1110", router)
	if err != nil{
		os.Exit(1)
	}
}

