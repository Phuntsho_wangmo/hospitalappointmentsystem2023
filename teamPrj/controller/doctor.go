package controller

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"teamPrj/model"
	"teamPrj/utils/httpResp"

	"github.com/gorilla/mux"
)


func AddDoctor(w http.ResponseWriter, r *http.Request) {
	var doc model.Doctor
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&doc)
	if err != nil {
		log.Println("Error decoding JSON:", err)
		httpResp.ResponseWithError(w, http.StatusBadRequest, "Invalid JSON data")
		return
	}

	dbErr := doc.Create()
	if dbErr != nil {
		log.Println("Error creating doctor:", dbErr)
		httpResp.ResponseWithError(w, http.StatusInternalServerError, dbErr.Error())
		return
	}

	httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"message": "Doctor data added"})
}



func getUserId(did string)(int64){
  ddid,_ := strconv.ParseInt(did, 10, 64)
  return ddid
}
func GetDoc(w http.ResponseWriter, r *http.Request){
  // if !VerifyCookie(w,r){
  //   return 
  // }
  //get url parameter
  did := mux.Vars(r)["did"]
  ddid := getUserId(did)
  doc := model.Doctor{Did: ddid}
  // fmt.Println(doc)
  getErr := doc.Read()
  if getErr != nil{
    switch getErr{
    case sql.ErrNoRows:
      httpResp.ResponseWithError(w, http.StatusNotFound, "doctor not found")
    default:
      httpResp.ResponseWithError(w, http.StatusInternalServerError, getErr.Error())
    }
  
  } else{
    httpResp.ResponseWithJson(w, http.StatusOK, doc)
  }

}

func UpdateDoc(w http.ResponseWriter, r *http.Request){
  // if !VerifyCookie(w,r){
  //   return 
  // }
  old_did := mux.Vars(r)["did"]
  old_dId:= getUserId(old_did)
  //student instance
  var doc model.Doctor
  //json object
  decoder := json.NewDecoder(r.Body)
  
  if err:= decoder.Decode(&doc); err != nil{
    httpResp.ResponseWithError(w, http.StatusBadRequest, "invalid json body")
    return
  }
  getErr := doc.Update(old_dId)
  if getErr != nil{
    switch getErr{
    case sql.ErrNoRows:
      httpResp.ResponseWithError(w, http.StatusNotFound, "doctor not found")
    default:
      httpResp.ResponseWithError(w, http.StatusInternalServerError, getErr.Error())
    }
  
  } else{
    httpResp.ResponseWithJson(w, http.StatusOK, doc)
  }
}

func DeleteDoc(w http.ResponseWriter, r *http.Request){
  // if !VerifyCookie(w,r){
  //   return 
  // }
  did := mux.Vars(r)["did"]
  ddid:= getUserId(did)
  d := model.Doctor{Did: ddid}
  //student instance
  if err:= d.Delete(); err != nil{
    httpResp.ResponseWithError(w, http.StatusBadRequest, err.Error())
    return
  }
  httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"status":"deleted"})
}

func GetAllDocs(w http.ResponseWriter, r *http.Request){
  // if !VerifyCookie(w, r){
  //   return 
  // }
  doctors, getErr := model.GetAllDoctors()
  if getErr != nil{
    httpResp.ResponseWithError(w, http.StatusBadRequest, getErr.Error())
    return
  }
  httpResp.ResponseWithJson(w, http.StatusOK, doctors)
}