package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"teamPrj/model"
	"teamPrj/utils/httpResp"
)

func CreateAppointment(w http.ResponseWriter, r *http.Request) {
	var appointment model.Appointment
	err := json.NewDecoder(r.Body).Decode(&appointment)
	if err != nil {
		log.Println(err)
		httpResp.ResponseWithError(w, http.StatusBadRequest, "Failed to decode request body")
		return
	}

	err = appointment.Create(int64(appointment.CID))
	if err != nil {
		log.Println(err)
		httpResp.ResponseWithError(w, http.StatusInternalServerError, "Failed to create appointment")
		return
	}

	// Return success response
	httpResp.ResponseWithJson(w, http.StatusCreated, map[string]string{"status": "Appointment created"})
}










// GetAppointmentByID retrieves an appointment by its ID
func GetAppointmentByID(w http.ResponseWriter, r *http.Request) {
	idParam := r.URL.Query().Get("id")
	appointmentID, err := strconv.Atoi(idParam)
	if err != nil {
		log.Println(err)
		httpResp.ResponseWithError(w, http.StatusBadRequest, "Invalid appointment ID")
		return
	}

	appointment, err := model.GetAppointmentByID(appointmentID)
	if err != nil {
		log.Println(err)
		httpResp.ResponseWithError(w, http.StatusInternalServerError, "Failed to get appointment")
		return
	}

	// Return success response
	httpResp.ResponseWithJson(w, http.StatusOK, appointment)
}

// GetAllAppointments retrieves all appointments
func GetAllAppointments(w http.ResponseWriter, r *http.Request) {
	appointments, err := model.GetAllAppointments()
	if err != nil {
		log.Println(err)
		httpResp.ResponseWithError(w, http.StatusInternalServerError, "Failed to get appointments")
		return
	}

	// Return success response
	httpResp.ResponseWithJson(w, http.StatusOK, appointments)
}

// UpdateAppointment updates an existing appointment
func UpdateAppointment(w http.ResponseWriter, r *http.Request) {
	var appointment model.Appointment
	err := json.NewDecoder(r.Body).Decode(&appointment)
	if err != nil {
		log.Println(err)
		httpResp.ResponseWithError(w, http.StatusBadRequest, "Failed to decode request body")
		return
	}

	err = appointment.Update()
	if err != nil {
		log.Println(err)
		httpResp.ResponseWithError(w, http.StatusInternalServerError, "Failed to update appointment")
		return
	}

	// Return success response
	httpResp.ResponseWithJson(w, http.StatusOK, appointment)
}

// DeleteAppointment deletes an appointment by its ID
func DeleteAppointment(w http.ResponseWriter, r *http.Request) {
	idParam := r.URL.Query().Get("id")
	appointmentID, err := strconv.Atoi(idParam)
	if err != nil {
		log.Println(err)
		httpResp.ResponseWithError(w, http.StatusBadRequest, "Invalid appointment ID")
		return
	}

	err = model.DeleteAppointment(appointmentID)
	if err != nil {
		log.Println(err)
		httpResp.ResponseWithError(w, http.StatusInternalServerError, "Failed to delete appointment")
		return
	}

	// Return success response
	httpResp.ResponseWithJson(w, http.StatusNoContent, map[string]string{"status": "appointment deleted"})
}
