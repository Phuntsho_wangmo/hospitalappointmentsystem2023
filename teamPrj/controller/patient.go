package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"teamPrj/model"
	"teamPrj/utils/httpResp"

	"github.com/gorilla/mux"
)

// AddPatient handles the creation of a new patient
func AddPatient(w http.ResponseWriter, r *http.Request) {

	var patient model.Patient

	err := json.NewDecoder(r.Body).Decode(&patient)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	// Perform additional validation or data manipulation if required

	err = model.AddPatient(&patient)
		if err != nil {
		httpResp.ResponseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"message": "Patient data added"})
}

// GetPatient retrieves a patient by ID
func GetPatient(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	patientID := params["pid"]

	pid, _ := strconv.ParseInt(patientID, 10, 64)

	patient, err := model.GetPatient(pid)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusNotFound, "Patient not found")
		return
	}

	httpResp.ResponseWithJson(w, http.StatusOK, patient)
}

// UpdatePatient updates a patient by ID
func UpdatePatient(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	patientID := params["pid"]

	// Convert patientID to int64
	pid, err := strconv.ParseInt(patientID, 10, 64)
	if err != nil {
		log.Println("Error parsing patient ID:", err)
		httpResp.ResponseWithError(w, http.StatusBadRequest, "Invalid patient ID")
		return
	}

	var updatedPatient model.Patient

	 decoder := json.NewDecoder(r.Body)
  
  if err:= decoder.Decode(&updatedPatient); err != nil{
    httpResp.ResponseWithError(w, http.StatusBadRequest, "invalid json body")
    return
  }

	// Set the patient ID for the updated patient
	updatedPatient.PatientID = pid

	err = model.UpdatePatient(&updatedPatient)
	if err != nil {
		log.Println("Error decoding JSON body:", err)
		httpResp.ResponseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"message": "Patient updated"})
}


// DeletePatient deletes a patient by ID
func DeletePatient(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	patientID := params["pid"]

	pid, _ := strconv.ParseInt(patientID, 10, 64)

	err := model.DeletePatient(pid)
	if err != nil {
		log.Println("Error updating patient:", err)
		httpResp.ResponseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"message": "Patient deleted"})
}

// GetAllPatients retrieves all patients
func GetAllPatients(w http.ResponseWriter, r *http.Request) {
	patients, err := model.GetAllPatients()
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	httpResp.ResponseWithJson(w, http.StatusOK, patients)
}

func GetAllPatientsByDoctorID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	doctorID := params["doctorID"]

	did, err := strconv.ParseInt(doctorID, 10, 64)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, "Invalid doctor ID")
		return
	}

	patients, err := model.GetAllPatientsByDoctorID(did)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	httpResp.ResponseWithJson(w, http.StatusOK, patients)
}

// GetPatientByDoctorIDAndAppointmentDate retrieves a patient by doctor ID and appointment date
func GetPatientByDoctorIDAndAppointmentDate(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	doctorID := params["doctorID"]
	appointmentDate := params["appointmentDate"]

	did, err := strconv.ParseInt(doctorID, 10, 64)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, "Invalid doctor ID")
		return
	}

	patient, err := model.GetPatientByDoctorIDAndAppointmentDate(did, appointmentDate)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	if patient == nil {
		httpResp.ResponseWithError(w, http.StatusNotFound, "Patient not found")
		return
	}

	httpResp.ResponseWithJson(w, http.StatusOK, patient)
}