package controller

import (
	"encoding/json"
	"net/http"
	"teamPrj/model"
	"teamPrj/utils/httpResp"
)

///---------------------------------------USER signup------------------------------------//
func SignupUsers(w http.ResponseWriter, r *http.Request) {

	var user model.Users

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	saveErr := user.Create()
	if saveErr != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	//no error
	httpResp.ResponseWithJson(w, http.StatusCreated, map[string]string{"status": "user created"})
}


func LoginUsers(w http.ResponseWriter, r *http.Request) {
	var user model.Users

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	usertype, err := model.Get(user.Email, user.Password)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusUnauthorized, err.Error())
		return
	}

	// Update the usertype value in the user object
	responseData := map[string]interface{}{
		"status":   "user login success",
		"userType": usertype,
	}

	httpResp.ResponseWithJson(w, http.StatusOK,responseData)
}



