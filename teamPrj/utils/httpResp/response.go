package httpResp

import (
	"encoding/json"
	"net/http"
)

func ResponseWithError(w http.ResponseWriter, code int,message string){
	ResponseWithJson(w, code, map[string]string{"error": message})

}

func ResponseWithJson(w http.ResponseWriter, code int, payload interface{}){
	//Marshalling
	// res_map := map[string]string {"error": dbErr.Error()}// error = alwasy string 
	response, _:= json.Marshal(payload)


	
	// to set header
	w.Header().Set("Content-Type","Appliaction/json")
	w.WriteHeader(code)//satu
	w.Write(response)// like 404 not found


}